#LiveOrderBoard

###Pre-Requisites

- Java JDK 8

###How to run the tests

- Linux/OS X: ```./gradlew clean test```
- Windows: ```gradlew clean test```

###How to build it

- Linux/OS X: ```./gradlew clean build```
- Windows: ```gradlew clean build```

###How to use the library

```kotlin
LiveOrderBoard().run {
    // register buy order
    registerOrder(
        order = Order(
            userId = "user1",
            orderQuantity = BigDecimal("3.5"),
            pricePerKg = BigDecimal("306"),
            orderType = OrderType.Buy
        )
    )

    // register sell order
    registerOrder(
        order = Order(
            userId = "user2",
            orderQuantity = BigDecimal("1.5"),
            pricePerKg = BigDecimal("307"),
            orderType = OrderType.Sell
        )
    )

    //cancel registered order
    cancelRegisteredOrder(
        orderId = registerOrder(
            order = Order(
                userId = "user3",
                orderQuantity = BigDecimal("2.0"),
                pricePerKg = BigDecimal("306"),
                orderType = OrderType.Sell
            )
        )!!
    )

    // summaryInformation
    summaryInformation
}
```



