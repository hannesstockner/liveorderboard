package com.silverbarsmarketplace.liveorderboard

import java.math.BigDecimal
import java.util.UUID

sealed class OrderType {
    object Buy : OrderType()
    object Sell : OrderType()
}

data class Order(
    val userId: String,
    val orderQuantity: BigDecimal,
    val pricePerKg: BigDecimal,
    val orderType: OrderType
)

data class SummaryInformation(
    val buyOrderSummaries: List<OrderSummary> = emptyList(),
    val sellOrderSummaries: List<OrderSummary> = emptyList()
) {
    data class OrderSummary(
        val orderQuantity: BigDecimal,
        val pricePerKg: BigDecimal
    )
}

class LiveOrderBoard {

    // in order to keep it simple I didn't make the class thread-safe
    // ie. by making the whole class immutable or
    // by synchronizing the properties/using ConcurrentHashMap

    private var orderById: Map<UUID, Order> = emptyMap()

    var summaryInformation: SummaryInformation = SummaryInformation()
        private set(value) {
            field = value
        }

    // instead of a nullable return type we could use an Either type with explicit error cases
    // for this exercise I didn't want to introduce additional libraries like Arrow or introduce more complex data types
    fun registerOrder(order: Order): UUID? =
        if (order.orderQuantity.signum() > 0 && order.pricePerKg.signum() > 0) {
            UUID.randomUUID().apply {
                orderById = orderById + (this to order)
                summaryInformation = generateSummaryInformation(orders = orderById.values)
            }
        } else {
            null
        }

    fun cancelRegisteredOrder(orderId: UUID): Order? =
        orderById[orderId].apply {
            orderById = orderById - orderId
            summaryInformation = generateSummaryInformation(orders = orderById.values)
        }

    // pure function could also be defined within the companion object or as a function outside of the class
    internal fun generateSummaryInformation(orders: Collection<Order>): SummaryInformation {
        fun List<Order>?.mergeOrders(): List<SummaryInformation.OrderSummary> {
            return this
                ?.groupBy { it.pricePerKg }
                ?.map { (pricePerKg, orders) ->
                    orders
                        .map { it.orderQuantity }
                        .reduce { sum, elem -> sum + elem }
                        .let { totalOrderQuantity ->
                            SummaryInformation.OrderSummary(
                                orderQuantity = totalOrderQuantity,
                                pricePerKg = pricePerKg
                            )
                        }
                } ?: emptyList()
        }

        return orders
            .groupBy { it.orderType }
            .let { ordersByOrderType ->
                SummaryInformation(
                    buyOrderSummaries = ordersByOrderType[OrderType.Buy]
                        .mergeOrders()
                        .sortedByDescending { it.pricePerKg },
                    sellOrderSummaries = ordersByOrderType[OrderType.Sell]
                        .mergeOrders()
                        .sortedBy { it.pricePerKg }
                )
            }
    }
}