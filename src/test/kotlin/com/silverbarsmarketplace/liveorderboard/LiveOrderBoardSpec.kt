package com.silverbarsmarketplace.liveorderboard

import io.kotlintest.matchers.collections.shouldBeEmpty
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.matchers.types.shouldBeNull
import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import io.kotlintest.specs.ShouldSpec
import java.math.BigDecimal
import java.util.UUID

internal class LiveOrderBoardSpec : ShouldSpec({

    should("register an order with valid properties") {
        val orderId = LiveOrderBoard().registerOrder(
            Order(
                userId = "user1",
                orderQuantity = BigDecimal("3.5"),
                pricePerKg = BigDecimal("306"),
                orderType = OrderType.Sell
            )
        )

        orderId.shouldNotBeNull()
    }

    should("not register an order with orderQuantity of 0") {
        val orderId = LiveOrderBoard().registerOrder(
            Order(
                userId = "user1",
                orderQuantity = BigDecimal("0"),
                pricePerKg = BigDecimal("306"),
                orderType = OrderType.Sell
            )
        )

        orderId.shouldBeNull()
    }

    should("not register an order with a negative orderQuantity") {
        val orderId = LiveOrderBoard().registerOrder(
            Order(
                userId = "user1",
                orderQuantity = BigDecimal("-1"),
                pricePerKg = BigDecimal("306"),
                orderType = OrderType.Sell
            )
        )

        orderId.shouldBeNull()
    }

    should("not register an order with pricePerKg of 0") {
        val orderId = LiveOrderBoard().registerOrder(
            Order(
                userId = "user1",
                orderQuantity = BigDecimal("3.5"),
                pricePerKg = BigDecimal("0"),
                orderType = OrderType.Sell
            )
        )

        orderId.shouldBeNull()
    }

    should("not register an order with a negative pricePerKg") {
        val orderId = LiveOrderBoard().registerOrder(
            Order(
                userId = "user1",
                orderQuantity = BigDecimal("3.5"),
                pricePerKg = BigDecimal("-1"),
                orderType = OrderType.Sell
            )
        )

        orderId.shouldBeNull()
    }

    should("not register an order with orderQuantity of 0 and pricePerKg of 0") {
        val orderId = LiveOrderBoard().registerOrder(
            Order(
                userId = "user1",
                orderQuantity = BigDecimal("0"),
                pricePerKg = BigDecimal("0"),
                orderType = OrderType.Sell
            )
        )

        orderId.shouldBeNull()
    }

    should("cancel a registered order") {
        val order = Order(
            userId = "user1",
            orderQuantity = BigDecimal("3.5"),
            pricePerKg = BigDecimal("306"),
            orderType = OrderType.Sell
        )

        val liveOrderBoard = LiveOrderBoard()
        val orderId = liveOrderBoard.registerOrder(order)

        val cancelledOrder = liveOrderBoard.cancelRegisteredOrder(orderId = orderId!!)

        cancelledOrder shouldBe order
    }

    should("not cancel an order for an orderId that was not registered") {
        val liveOrderBoard = LiveOrderBoard()
        val cancelledOrder = liveOrderBoard.cancelRegisteredOrder(orderId = UUID.randomUUID())

        cancelledOrder.shouldBeNull()
    }

    should("return a SummaryInformation with merged sell orders and no buy orders") {
        val liveOrderBoard = LiveOrderBoard().apply {
            registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Sell))
            registerOrder(Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Sell))
            registerOrder(Order("user3", BigDecimal("1.5"), BigDecimal("307"), OrderType.Sell))
            registerOrder(Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Sell))
        }

        val expected = SummaryInformation(
            buyOrderSummaries = emptyList(),
            sellOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("5.5"), pricePerKg = BigDecimal("306")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.2"), pricePerKg = BigDecimal("310"))
            )
        )

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.sellOrderSummaries.shouldContainExactly(expected.sellOrderSummaries)
        summaryInformation.buyOrderSummaries.shouldBeEmpty()
    }

    should("return a SummaryInformation with merged buy orders and no sell orders") {
        val liveOrderBoard = LiveOrderBoard().apply {
            registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Buy))
            registerOrder(Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Buy))
            registerOrder(Order("user3", BigDecimal("1.5"), BigDecimal("307"), OrderType.Buy))
            registerOrder(Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Buy))
        }

        val expected = SummaryInformation(
            buyOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.2"), pricePerKg = BigDecimal("310")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("5.5"), pricePerKg = BigDecimal("306"))
            ),
            sellOrderSummaries = emptyList()
        )

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.buyOrderSummaries.shouldContainExactly(expected.buyOrderSummaries)
        summaryInformation.sellOrderSummaries.shouldBeEmpty()
    }

    should("return a SummaryInformation with buy order and sell order of same price") {
        val liveOrderBoard = LiveOrderBoard().apply {
            registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Buy))
            registerOrder(Order("user1", BigDecimal("3.0"), BigDecimal("306"), OrderType.Sell))
        }

        val expected = SummaryInformation(
            buyOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("3.5"), pricePerKg = BigDecimal("306"))
            ),
            sellOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("3.0"), pricePerKg = BigDecimal("306"))
            )
        )

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.buyOrderSummaries.shouldContainExactly(expected.buyOrderSummaries)
        summaryInformation.sellOrderSummaries.shouldContainExactly(expected.sellOrderSummaries)
    }

    should("return a SummaryInformation with sell order of same price from same user") {
        val liveOrderBoard = LiveOrderBoard().apply {
            registerOrder(Order("user1", BigDecimal("1.2"), BigDecimal("310"), OrderType.Sell))
            registerOrder(Order("user1", BigDecimal("1.5"), BigDecimal("310"), OrderType.Sell))
        }

        val expected = SummaryInformation(
            buyOrderSummaries = emptyList(),
            sellOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("2.7"), pricePerKg = BigDecimal("310"))
            )
        )

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.buyOrderSummaries.shouldBeEmpty()
        summaryInformation.sellOrderSummaries.shouldContainExactly(expected.sellOrderSummaries)
    }

    should("return a SummaryInformation with cancelled buy and sell orders") {
        val liveOrderBoard = LiveOrderBoard().apply {
            cancelRegisteredOrder(registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Buy))!!)
            cancelRegisteredOrder(registerOrder(Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Sell))!!)
        }

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.sellOrderSummaries.shouldBeEmpty()
        summaryInformation.buyOrderSummaries.shouldBeEmpty()
    }

    should("return a SummaryInformation with cancelled sell orders and no buy orders") {
        val liveOrderBoard = LiveOrderBoard().apply {
            registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Sell))
            registerOrder(Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Sell))
            registerOrder(Order("user3", BigDecimal("1.5"), BigDecimal("307"), OrderType.Sell))
            val orderId = registerOrder(Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Sell))
            cancelRegisteredOrder(orderId!!)
        }

        val expected = SummaryInformation(
            buyOrderSummaries = emptyList(),
            sellOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("3.5"), pricePerKg = BigDecimal("306")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.2"), pricePerKg = BigDecimal("310"))
            )
        )

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.sellOrderSummaries.shouldContainExactly(expected.sellOrderSummaries)
        summaryInformation.buyOrderSummaries.shouldBeEmpty()
    }

    should("return a SummaryInformation with merged buy orders and merged sell orders") {
        val liveOrderBoard = LiveOrderBoard().apply {
            registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Buy))
            registerOrder(Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Buy))
            registerOrder(Order("user3", BigDecimal("1.5"), BigDecimal("307"), OrderType.Buy))
            registerOrder(Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Buy))

            registerOrder(Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Sell))
            registerOrder(Order("user2", BigDecimal("7.2"), BigDecimal("310"), OrderType.Sell))
            registerOrder(Order("user3", BigDecimal("9.5"), BigDecimal("307"), OrderType.Sell))
            registerOrder(Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Sell))
        }

        val expected = SummaryInformation(
            buyOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.2"), pricePerKg = BigDecimal("310")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("5.5"), pricePerKg = BigDecimal("306"))
            ),
            sellOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("5.5"), pricePerKg = BigDecimal("306")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("9.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("7.2"), pricePerKg = BigDecimal("310"))
            )
        )

        val summaryInformation = liveOrderBoard.summaryInformation

        summaryInformation.buyOrderSummaries.shouldContainExactly(expected.buyOrderSummaries)
        summaryInformation.sellOrderSummaries.shouldContainExactly(expected.sellOrderSummaries)
    }

    should("generate SummaryInformation from buy and sell orders") {
        val summaryInformation = LiveOrderBoard().generateSummaryInformation(
            listOf(
                Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Buy),
                Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Buy),
                Order("user3", BigDecimal("1.5"), BigDecimal("307"), OrderType.Buy),
                Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Buy),
                Order("user5", BigDecimal("0.1"), BigDecimal("306"), OrderType.Buy),

                Order("user1", BigDecimal("3.5"), BigDecimal("306"), OrderType.Sell),
                Order("user2", BigDecimal("1.2"), BigDecimal("310"), OrderType.Sell),
                Order("user3", BigDecimal("1.5"), BigDecimal("307"), OrderType.Sell),
                Order("user4", BigDecimal("2.0"), BigDecimal("306"), OrderType.Sell),
                Order("user5", BigDecimal("0.2"), BigDecimal("306"), OrderType.Sell)
            )
        )

        val expected = SummaryInformation(
            buyOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.2"), pricePerKg = BigDecimal("310")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("5.6"), pricePerKg = BigDecimal("306"))
            ),
            sellOrderSummaries = listOf(
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("5.7"), pricePerKg = BigDecimal("306")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.5"), pricePerKg = BigDecimal("307")),
                SummaryInformation.OrderSummary(orderQuantity = BigDecimal("1.2"), pricePerKg = BigDecimal("310"))
            )
        )

        summaryInformation.buyOrderSummaries.shouldContainExactly(expected.buyOrderSummaries)
        summaryInformation.sellOrderSummaries.shouldContainExactly(expected.sellOrderSummaries)
    }
})